<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4>Avaliable requests</h4>
                    <pre>
/api/auth - POST (login and password fields are required)
                        
/api/projects/ - GET, POST
/api/projects/<id> - GET, PATCH, PUT, DELETE
/api/projects/my - GET
/api/projects/own - GET

/api/tasks/ - GET, POSTS
/api/tasks/<id> - GET, PATCH, PUT, DELETE
/api/tasks/my - GET
/api/tasks/own - GET

/api/users/<id> - GET, PATCH
                    </pre>
                </div>
                <div class="col-md-6">
                    <h4>Avaliable users</h4>
                    <pre>
ivan 123123 ; token: 477kp7hGKBiJ_-UWPhcqP2CBpEyDi8MN
ivan1 321321 ; token: EDA3HjaM774NuoDQJPl6llO6QUAbaqqx
                    </pre>
                </div>  
            </div>
            <form id="rest_form">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Requested method</h4>
                        <select name="http_method" class="form-control">
                        </select>
                        <input type="text" name="requested_method" placeholder="Requested method" class="form-control">
                    </div>
                    <div class="col-md-8">
                        <h4>Target URL</h4>
                        <input type="text" name="uri" disabled="disabled" value="http://127.0.0.1/api/" class="form-control"/>
                        <input type="submit" class="btn btn-success">
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h4>Authentication</h4>
                        <input type="radio" name="authentication" value="basic" id="authentication_basic" checked="checked"/>
                        <label for="authentication_basic">Basic</label><br/>
                        <input type="radio" name="authentication" value="bearer" id="authentication_bearer"/>
                        <label for="authentication_basic">Token</label><br/>
                        
                        <div class="container__auth_basic">
                            <label for="login">Login</label>
                            <input type="text" name="login" class="form-control" />
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" />
                        </div>
                        
                        <div class="container__auth_token">
                            <label for="token">Token</label>
                            <input type="text" name="token" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h4>Request Data</h4>
                        <table class="table table-striped" id="fields_table">
                            <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Value</th>
                                    <th>
                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" class="add_text_field_btn">Text</a></li>
                                                    <li><a href="#" class="add_file_field_btn">File</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-8">
                        <h4>Request answer</h4>
                        <pre id="answer"></pre>
                    </div>  
                </div>
            </form>
        </div>
    </div>
</div>

<?
$baseUrl = Url::toRoute('/api/', true);
$script = <<< JS
    $('#rest_form').restForm({baseUrl : '$baseUrl/'});
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
