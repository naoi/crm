<?php

namespace frontend\controllers\api;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use common\models\User;

class BaseController extends \yii\rest\ActiveController
{   
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                [
                    'class' => HttpBasicAuth::className(),
                    'auth' => function($login, $password) {
                        return $this->authenticate($login, $password);
                    }
                ],
                HttpBearerAuth::className(),
            ],
        ];
        return $behaviors;
    }
    
    private function authenticate($login, $password)
    {
        if (empty($login) || empty($password)) {
            return null;
        }
        
        $user = User::findByLogin($login);
        
        if (!$user || !$user->validatePassword($password)) {
            return null;
        }
        
        return $user;
    }
}
