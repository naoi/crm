<?php

namespace frontend\controllers\api;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use frontend\models\api\Task;
use yii\helpers\Url;
use Yii;

class TaskController extends BaseController
{
    public $modelClass = 'frontend\models\api\Task';
    
     public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        $actions['delete'] = [
            'class' => 'frontend\controllers\api\actions\DeleteAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkOwnerAccess'],
        ];
        $actions['index'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
        ];
        $actions['update'] = [
            'class' => 'frontend\controllers\api\actions\UpdateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkOwnerAccess'],
        ];
        $actions['my'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
            'type' => 'my',
        ];
        $actions['own'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
            'type' => 'own',
        ];

        return $actions;
    }
    
    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['my'] = ['GET'];
        $verbs['own'] = ['GET'];
    }
    
    public function actionCreate()
    {
        $model = new Task();
        $model->scenario = 'create';
        $model->status = Task::STATUS_NEW;
        
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $model->refresh();
        } elseif (!$model->hasErrors()) {
            throw new \yii\web\ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
    
    public function checkOwnerAccess($id, $model)
    {
        if ($model->project->owner_id == Yii::$app->user->id) {
            return true;
        }
        
         throw new \yii\web\ForbiddenHttpException('Only owner is able to delete or update');
    }
}
