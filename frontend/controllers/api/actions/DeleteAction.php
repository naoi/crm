<?php

namespace frontend\controllers\api\actions;

use Yii;
use yii\web\ServerErrorHttpException;
use yii\rest\DeleteAction as BaseAction;

class DeleteAction extends BaseAction
{
    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        if ($model->delete() === false && !$model->is_deleted) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }
}
