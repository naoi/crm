<?php

namespace frontend\controllers\api\actions;

use Yii;
use yii\web\ServerErrorHttpException;
use yii\data\ActiveDataProvider;
use yii\rest\IndexAction as BaseAction;

class IndexAction extends BaseAction
{
    public $type = false;
    
    protected function prepareDataProvider()
    {
        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;

        $query = $modelClass::find()->active();
        if ($this->type == 'my') {
            $query = $query->my(Yii::$app->user->id);
        } else if ($this->type == 'own') {
            $query = $query->own(Yii::$app->user->id);
        }
        
        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $query,
        ]);
    }
}
