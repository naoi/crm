<?php

namespace frontend\controllers\api;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use frontend\models\api\Project;
use yii\helpers\Url;
use Yii;

class ProjectController extends BaseController
{
    public $modelClass = 'frontend\models\api\Project';
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        $actions['delete'] = [
            'class' => 'frontend\controllers\api\actions\DeleteAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkOwnerAccess'],
        ];
        $actions['index'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
        ];
        $actions['my'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
            'type' => 'my',
        ];
         $actions['own'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
            'type' => 'own',
        ];
        $actions['update'] = [
            'class' => 'frontend\controllers\api\actions\UpdateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkOwnerAccess'],
        ];

        return $actions;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['my'] = ['GET'];
        $verbs['own'] = ['GET'];
    }

    
    public function actionCreate()
    {
        $model = new Project();
        $model->scenario = 'create';
        $model->status = Project::STATUS_NEW;
        $model->owner_id = Yii::$app->user->id;
        
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $model->refresh();
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
    
    public function checkOwnerAccess($id, $model)
    {
        if ($model->owner_id == Yii::$app->user->id) {
            return true;
        }
        
         throw new \yii\web\ForbiddenHttpException('Only owner is able to delete or update');
    }
}
