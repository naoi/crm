<?php

namespace frontend\controllers\api;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use common\models\User;

class AuthController extends \yii\rest\Controller
{
    public function actionAuth()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $login = Yii::$app->request->post('login', false);
        $password = Yii::$app->request->post('password', false);
        
        if (!$login || !$password) {
            throw new \yii\web\NotAcceptableHttpException('Empty data');
        }
        
        $user = User::findByLogin($login);
        if ($user->validatePassword($password)) {
            return $user->auth_key;
        }
        
        throw new \yii\web\ForbiddenHttpException('Only owner is able to delete or update');
    }
}
