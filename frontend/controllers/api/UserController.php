<?php

namespace frontend\controllers\api;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use common\models\User;

class UserController extends BaseController
{
    public $modelClass = 'frontend\models\api\User';
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        
        $actions['index'] = [
            'class' => 'frontend\controllers\api\actions\IndexAction',
            'modelClass' => $this->modelClass,
        ];

        return $actions;
    }

    public function actionUpdate($id)
    {
        $model = User::findIdentity($id);
        if (!$model) {
            throw new NotFoundHttpException("Object not found: $id");
        }
        
        $this->checkOwnerAccess($id, $model);

        $files = $this->parseRawFiles();
        if (isset($files['avatar']) && !empty($files['avatar'])) {
            foreach ($files['avatar'] as $name => $data) {
                $model->upload($name, $data);
                break;
            }
        }
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new \yii\web\ServerErrorHttpException('Failed to update the object for unknown reason.');
        }
        
        if ($model->image) {
            $model->upload();
        }
        
        $model->refresh();

        return $model;
    }
    
    public function checkOwnerAccess($id, $model)
    {
        if ($model->id == Yii::$app->user->id) {
            return true;
        }
        
         throw new \yii\web\ForbiddenHttpException('Only owner is able to delete or update');
    }
    
    /**
    * Parse raw HTTP request data
    *
    * Pass in $a_data as an array. This is done by reference to avoid copying
    * the data around too much.
    *
    * Any files found in the request will be added by their field name to the
    * $data['files'] array.
    *
    * @param   array  Empty array to fill with data
    * @return  array  Associative array of request data
    */
    private function parseRawFiles()
    {
        $files = [];
        // read incoming data
        $input = file_get_contents('php://input');
       
        // grab multipart boundary from content type header
        preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
       
        // content type is probably regular form-encoded
        if (!count($matches)) {
            // we expect regular puts to containt a query string containing data
            parse_str(urldecode($input), $a_data);
            return $files;
        }
       
        $boundary = $matches[1];
       
        // split content by boundary and get rid of last -- element
        $a_blocks = preg_split("/-+$boundary/", $input);
        array_pop($a_blocks);
       
        // loop data blocks
        foreach ($a_blocks as $id => $block) {
            if (empty($block)) {
                continue;
            }
       
            // you'll have to var_dump $block to understand this and maybe replace \n or \r with a visibile char
       
            // parse uploaded files
            if (strpos($block, '; filename=') !== FALSE) {
                // match "name", then everything after "stream" (optional) except for prepending newlines
                preg_match("/name=\"([^\"]*)\"/s", $block, $matches);
                $name = $matches[1];
                preg_match("/filename=\"([^\"]*)\"/s", $block, $matches);
                $filename = $matches[1];
                
                $dataStart = 0;
                for ($i = 0; $i < strlen($block) - 4; $i++) {
                    if ((ord($block{$i}) == 0xD
                         && ord($block{$i + 1}) == 0xA
                         && ord($block{$i + 2}) == 0xD
                         && ord($block{$i + 3}) == 0xA
                       ) || (
                         ord($block{$i}) == 0xA)
                         && ord($block{$i + 1}) == 0xD
                         && ord($block{$i + 2}) == 0xA
                         && ord($block{$i + 3}) == 0xD
                       ) {
                        $dataStart = $i + 4;
                        break;
                    }
                }
                
                $files[$name][$filename] = substr($block, $dataStart);
            }
        }
        
        return $files;
    }
}
