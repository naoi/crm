(function ($) {
    $.fn.restForm = function (options) {
        var settings = $.extend({            
            'baseUrl': 'http://127.0.0.1/api/',
            'httpMethods': [
                'GET',
                'POST',
                'PATCH',
                'PUT',
                'DELETE'
            ]
        }, options);
        
        return this.each(function () {
            var $this = $(this);
            
            initMethods($this);
            initSelects($this);
            initTableButtons($this);
            initFormSubmit($this);
        });
    
        function initSelects($target) {
            var $requestedMethod = $target.find('input[name="requested_method"]');
            var $resultUrl = $target.find('input[name="uri"]');
            
            $requestedMethod.on('change', $requestedMethod, function() {
                $resultUrl.val(settings.baseUrl + $(this).val());
            });
            
            var $authMethod = $target.find('input[name="authentication"]');
            
            $authMethod.on('change', $authMethod, function() {
                if ($(this).val() == 'basic') {
                    $target.find('.container__auth_basic').show();
                    $target.find('.container__auth_token').hide();
                } else if ($(this).val() == 'bearer') {
                    $target.find('.container__auth_token').show();
                    $target.find('.container__auth_basic').hide();
                }
            });
            
            $authMethod.eq(0).trigger('change');
        }
    
        function initMethods($target) {
            var $httpMethods = $target.find('select[name="http_method"]');
            
            for (var i in settings.httpMethods) {
                $httpMethods.append(new Option(settings.httpMethods[i], settings.httpMethods[i], i == 0, false));
            }
        }
        
        function initTableButtons($target) {
            var $addTextButton = $target.find('.add_text_field_btn');
            var $addFileButton = $target.find('.add_file_field_btn');
            var $removeButton = $target.find('.remove_field_btn');
            var $tableBody = $target.find('table#fields_table tbody');
            
            function addRow(type = text) {
                var row = ' \
                    <tr> \
                        <td> \
                            <input type="text" name="keys[]" placeholder="Key"> \
                        </td> \
                        <td> \
                            <input type="' + type + '" name="values[]" placeholder="Value"> \
                        </td> \
                        <td> \
                            <button type="button" class="pull-right remove_field_btn"> \
                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> \
                            </button> \
                        </td> \
                    </tr> \
                ';
                
                $tableBody.append(row);
            }
            
            $addTextButton.on('click', $addTextButton, function() {
                addRow('text');
            });
            
            $addFileButton.on('click', $addFileButton, function() {
                addRow('file');
            });
            
            $(document).on('click', 'table#fields_table .remove_field_btn' , function() {
                $(this).closest('tr').remove();
            });
        }
        
        function initFormSubmit($target) {
            $target.on('submit', function(e) {
                e.preventDefault();
                
                sendQuery(prepareRequest($target), $target);
            });
        }
        
        function prepareRequest($target) {
            var request = {
                httpMethod : $target.find('select[name="http_method"]').val(),
                method : $target.find('input[name="requested_method"]').val(),
                url : $target.find('input[name="uri"]').val(),
                auth : $target.find('input[name="authentication"]:checked').val(),
                login : $target.find('input[name="login"]').val(),
                password : $target.find('input[name="password"]').val(),
                token : $target.find('input[name="token"]').val(),
                fields : new FormData()
            };
            
            $target.find('table#fields_table tbody tr').each(function () {
                var $this = $(this);
                var key = $this.find('input[name="keys[]"]').val();
                var value = $this.find('input[name="values[]"]').val();
                var $input = $this.find('input[name="values[]"]');
                
                key = key.trim();
                value = value.trim();
                
                if ($input.attr('type') == 'file') {
                    if ($input[0].files && $input[0].files[0]) {
                        request.fields.append(key, $input[0].files[0]);
                    };
                } else if (value.length) {
                    request.fields.append(key, value);
                }
            });
            
            return request;
        }
        
        function sendQuery(request, $target) {
            $.ajax({
                type : request.httpMethod,
                url : request.url,
               // dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                data : request.fields,
                beforeSend: function (xhr) {
                    if (request.auth == 'basic') {
                        xhr.setRequestHeader ("Authorization", "Basic " + btoa(request.login + ":" + request.password));
                    } else if (request.auth == 'bearer') {
                        xhr.setRequestHeader ("Authorization", "Bearer " + request.token);
                    }
                },
                success: function (data, status, jqXHR) {
                    try {
                        $target.find('#answer').html(jqXHR.status + '<br/>' + JSON.stringify(data, null, 4));
                    } catch (e) {
                        $target.find('#answer').html(jqXHR.status + '<br/>' + data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    try {
                        $target.find('#answer').html(jqXHR.status + '<br/>' + textStatus + '<br/>' + JSON.stringify(jqXHR.responseJSON, null, 4));
                    } catch (e) {
                        $target.find('#answer').html(jqXHR.status + '<br/>' + textStatus + '<br/>' + errorThrown);
                    }
                }
            });
        }
    };
})(jQuery);
