<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser',
            ],
            'baseUrl' => '/',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'enableSession' => false,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'GET,POST /' => 'site/index',
                'GET,POST /site/<action>' => 'site/<action>',
                'GET,POST /api/auth/<action>' => 'api/auth/<action>',
                'POST /api/auth/' => 'api/auth/auth',
                'GET /api/projects/my' => 'api/project/my',
                'GET /api/projects/own' => 'api/project/own',
                'GET /api/tasks/my' => 'api/task/my',
                'GET /api/tasks/own' => 'api/task/own',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'api/user',
                        'api/project',
                        'api/task',
                    ],
                    'prefix' => '/',
                ],
            ],
        ],
        
    ],
    'params' => $params,
];
