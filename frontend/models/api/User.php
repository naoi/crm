<?
namespace frontend\models\api;

use Yii;
use common\models\User as BaseUser;

class User extends BaseUser
{
    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'avatarUrl';
        return $fields;
    }
    
    public function extraFields()
    {
        return ['avatarUrl'];
    }
}