<?
namespace frontend\models\api;

use Yii;
use common\models\Task as BaseTask;

class Task extends BaseTask
{
    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'reassignLogs';
        return $fields;
    }
    
    public function extraFields()
    {
        return ['reassingLogs'];
    }
}

