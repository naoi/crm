<?php

namespace common\models;

use Yii;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\models\ReassignLog;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property integer $project_id
 * @property integer $executor_id
 * @property integer $status
 * @property string $deadline
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $is_deleted
 *
 * @property ReassignLog[] $reassignLogs
 * @property User $executor
 * @property Project $project
 */
class Task extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_WORK = 2;
    const STATUS_REJECTED = 3;
    const STATUS_FINISHED = 4;
    
    const STATUSES = [
        self::STATUS_NEW => 'New',
        self::STATUS_WORK => 'In work',
        self::STATUS_REJECTED => 'Rejected',
        self::STATUS_FINISHED => 'Finished',
    ];
    
    public $reassignReason = false;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestmapBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                    'deleted_at' => new Expression('NOW()'),
                ],
                'replaceRegularDelete' => true,
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'project_id', 'executor_id', 'deadline'], 'required', 'on' => 'create'],
            [['project_id', 'executor_id', 'status'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['created_at', 'updated_at', 'deleted_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['deadline'], 'datetime', 'format' => 'php:Y-m-d'],
            [['name', 'reassignReason'], 'string', 'max' => 255],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['executor_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'project_id' => 'Project ID',
            'executor_id' => 'Executor ID',
            'status' => 'Status',
            'deadline' => 'Deadline',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->deadline = date('Y-m-d', strtotime($this->deadline));
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        $this->deadline = date('Y-m-d', strtotime($this->deadline));
        
        if ($this->getOldAttribute('executor_id') != $this->executor_id) {
            $log = new ReassignLog();
            $log->task_id = $this->id;
            $log->user_new_id = $this->executor_id;
            $log->user_old_id = $this->getOldAttribute('executor_id');
            $log->reason = $this->reassignReason;
            $log->save();
        }
        
        return true;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReassignLogs()
    {
        return $this->hasMany(ReassignLog::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(User::className(), ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\TaskQuery(get_called_class());
    }
}
