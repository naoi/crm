<?php

namespace common\models;

use Yii;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "reassign_log".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $user_old_id
 * @property integer $user_new_id
 * @property string $reason
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Task $task
 * @property User $userNew
 * @property User $userOld
 */
class ReassignLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestmapBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reassign_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'user_new_id'], 'required'],
            [['task_id', 'user_old_id', 'user_new_id'], 'integer'],
            [['reason'], 'string'],
            [['created_at', 'updated_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['user_new_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_new_id' => 'id']],
            [['user_old_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_old_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'user_old_id' => 'User Old ID',
            'user_new_id' => 'User New ID',
            'reason' => 'Reason',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNew()
    {
        return $this->hasOne(User::className(), ['id' => 'user_new_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOld()
    {
        return $this->hasOne(User::className(), ['id' => 'user_old_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ReassignLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ReassignLogQuery(get_called_class());
    }
}
