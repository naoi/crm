<?php

namespace common\models;

use Yii;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $owner_id
 * @property integer $status
 * @property string $deadline
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $is_deleted
 *
 * @property User $owner
 * @property Task[] $tasks
 */
class Project extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_WORK = 2;
    const STATUS_REJECTED = 3;
    const STATUS_FINISHED = 4;
    
    const STATUSES = [
        self::STATUS_NEW => 'New',
        self::STATUS_WORK => 'In work',
        self::STATUS_REJECTED => 'Rejected',
        self::STATUS_FINISHED => 'Finished',
    ];
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestmapBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                    'deleted_at' => new Expression('NOW()'),
                ],
                'replaceRegularDelete' => true,
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'owner_id', 'status', 'deadline'], 'required', 'on' => 'create'],
            [['description'], 'string'],
            [['owner_id', 'status'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['created_at', 'updated_at', 'deleted_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['deadline'], 'datetime', 'format' => 'php:Y-m-d'],
            [['name'], 'string', 'max' => 255],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'owner_id' => 'Owner ID',
            'status' => 'Status',
            'deadline' => 'Deadline',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->deadline = date('Y-m-d', strtotime($this->deadline));
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        $this->deadline = date('Y-m-d', strtotime($this->deadline));
        return true;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ProjectQuery(get_called_class());
    }
}
