<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Project]].
 *
 * @see \common\models\Project
 */
class ProjectQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['project.is_deleted' => false]);
    }
    
    public function my($userId)
    {
        if ($userId) {
            $this->joinWith('tasks');
            $this->andWhere([
                'or',
                ['owner_id' => $userId],
                ['task.executor_id' => $userId],
            ]);
        }
        return $this;
    }
    
    public function own($userId)
    {
        if ($userId) {
            $this->andWhere(['owner_id' => $userId]);
        }
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Project[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Project|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
