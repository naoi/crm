<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\User]].
 *
 * @see \common\models\User
 */
class UserQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_deleted' => false]);
    }
    
    public function authKey($key)
    {
        return $this->andWhere(['auth_key' => $key]);
    }
    
    public function login($login)
    {
        return $this->andWhere(['login' => $login]);
    }
    
    public function id($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @inheritdoc
     * @return \common\models\User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
