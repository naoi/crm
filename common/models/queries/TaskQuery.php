<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Task]].
 *
 * @see \common\models\Task
 */
class TaskQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['task.is_deleted' => false]);
    }
    
    public function own($userId)
    {
        if ($userId) {
            $this->joinWith('project');
            $this->andWhere(['owner_id' => $userId]);
        }
        return $this;
    }
    
    public function my($userId)
    {
        if ($userId) {
            $this->andWhere(['executor_id' => $userId]);
        }
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Task[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Task|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
