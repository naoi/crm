<?php

namespace common\models;

use Yii;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $birth_date
 * @property string $avatar
 * @property string $auth_key
 * @property string $token
 * @property string $password_hash
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property boolean $is_deleted
 * 
 * @property Project[] $projects
 * @property ReassignLog[] $reassignLogs
 * @property ReassignLog[] $reassignLogs0
 * @property Task[] $tasks
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $image = false;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestmapBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                    'deleted_at' => new Expression('NOW()'),
                ],
                'replaceRegularDelete' => true,
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'birth_date', 'avatar', 'auth_key', 'password_hash', 'login'], 'required'],
            [['is_deleted'], 'boolean'],
            [['created_at', 'updated_at', 'deleted_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['birth_date'], 'datetime', 'format' => 'php:Y-m-d'],
            [['name', 'avatar', 'password_hash', 'login'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['login'], 'unique'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birth_date' => 'Birth Date',
            'avatar' => 'Avatar',
            'auth_key' => 'Auth Key',
            'token' => 'Token',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReassignLogs()
    {
        return $this->hasMany(ReassignLog::className(), ['user_new_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReassignLogs0()
    {
        return $this->hasMany(ReassignLog::className(), ['user_old_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['executor_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\UserQuery(get_called_class());
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()->active()->id($id)->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if ($type == 'yii\filters\auth\HttpBasicAuth') {
            return static::find()->active()->login($token)->one();    
        } else if ($type == 'yii\filters\auth\HttpBearerAuth') {
            return static::find()->active()->authKey($token)->one();
        }
    }

    /**
     * Finds user by login
     *
     * @param string $username
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::find()->active()->login($login)->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public function upload($filename, $data)
    {
        $dir = Yii::getAlias('@frontend') . '/web/uploads/user/' . $this->id;
        if (!file_exists($dir)) {
            FileHelper::createDirectory($dir);
        }
        
        $f = fopen($dir.'/'.$filename, 'wb');
        $result = fwrite($f, $data);
        
        if ($result && $this->avatar && $this->avatar != $filename) {
            $this->removeImage($this->avatar);
        }
        
        $this->avatar = $filename;
    }
    
    public function removeImage($oldImage = null)
    {
        if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/user/' . $this->id . "/" . $oldImage)) {
            unlink(Yii::getAlias('@frontend') . '/web/uploads/user/' . $this->id . "/" . $oldImage);
        }
    }

    public function getAvatarUrl()
    {
        return '/uploads/user/' . $this->id . '/' . $this->avatar;
    }
    
    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->birth_date = date('Y-m-d', strtotime($this->birth_date));
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        $this->birth_date = date('Y-m-d', strtotime($this->birth_date));
        return true;
    }
}