<?php

use yii\db\Migration;

class m170726_191711_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'owner_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'deadline' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'project_id' => $this->integer()->notNull(),
            'executor_id' => $this->integer()->notNull(),
            'status' => $this->integer(),
            'deadline' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        
        $this->createTable('reassign_log', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->notNull(),
            'user_old_id' => $this->integer()->null(),
            'user_new_id' => $this->integer()->notNull(),
            'reason' => $this->text()->null(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
        
        $this->addForeignKey('fk_project_owner', 'project', 'owner_id', 'user', 'id', 'RESTRICT');
        
        $this->addForeignKey('fk_task_project', 'task', 'project_id', 'project', 'id', 'RESTRICT');
        $this->addForeignKey('fk_task_executor', 'task', 'executor_id', 'user', 'id', 'RESTRICT');
        
        $this->addForeignKey('fk_reassign_log_task', 'reassign_log', 'task_id', 'task', 'id', 'CASCADE');
        $this->addForeignKey('fk_reassign_log_user_old', 'reassign_log', 'user_old_id', 'user', 'id', 'RESTRICT');
        $this->addForeignKey('fk_reassign_log_user_new', 'reassign_log', 'user_new_id', 'user', 'id', 'RESTRICT');   
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_project_owner', 'project');
        
        $this->dropForeignKey('fk_task_project', 'task');
        $this->dropForeignKey('fk_task_executor', 'task');
        
        $this->dropForeignKey('fk_reassign_log_task', 'reassign_log');
        $this->dropForeignKey('fk_reassign_log_user_old', 'reassign_log');
        $this->dropForeignKey('fk_reassign_log_user_new', 'reassign_log');
        
        $this->dropTable('project');
        $this->dropTable('task');
        $this->dropTable('reassign_log');
                
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_191711_init cannot be reverted.\n";

        return false;
    }
    */
}
